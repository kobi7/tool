import Sys.*;



class Vmvc
{

	static function main()
	{
		var welcome = "Welcome to vmvc tool.";
		println(welcome);
		
		var args = Sys.args();
		if (args.length == 0) Aux.usage();
		else { Aux.show_args(args);
				 do_args(args); }
	}



	static function do_args(args:Array<String>)
	{
		// trace("do_args");
		Utils.assert(args.length>0);
		var first:String = args[0];
		switch(first){
			case "gen":
				do_gen(args);
			case "example":
				ExampleReader.do_example(args);
			default:
				// throw 'unknown argument: $first';
				println( 'unknown argument: $first');
				Aux.usage();

		}

	}
// ============== do
	private static function do_gen(args:Array<String>)
	{
		var arg2 = args[1];
		switch(arg2) //after gen
		{
			case "ctrl":
				GenCtrl.do_gen_ctrl();
			case "domain":
				GenDomain.do_gen_domain(args);
			case "views":
				GenViews.do_gen_views();
			case "init":
				GenFolders.do_gen_init();

		}

	}
	
	

}