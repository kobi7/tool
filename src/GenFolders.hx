import sys.FileSystem.*;
import Sys.*;


class GenFolders
{
	
	
	
	public static function do_gen_init(dummy:Bool=false)
	{
		function create_folder (at:String, folder:String):Void
		{
			try {
				
				//var p = Path.combine(at,folder);
				println("making folder: "+folder);
				println('at $at.');
				var p:String = '${at}/${folder}';
				p.
				// todo: assert no // in the middle of p. 
				createDirectory(p);
			}
			catch (msg:String)
			{
				trace(msg);
			}
		}
		
		function create_folders(at:String, subfolders:Array<String>)
		{
				for (item in subfolders)
				{
					create_folder(at, item );
				}
				
		}
			

		println("do_gen_init");
		println("will now create folders in the current directory: domain, ctrl, views, tests");
		var list = ["domain", "ctrl", "views", "tests" ];
		
		var folder:String = ".";
		if (dummy) folder= "./vmvc_test/";
		create_folders(".", list);
		}
		
}