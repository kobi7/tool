import Sys.*;

// reads the examples.txt file into lines, validates the commands against regex, then creates a list of "paths+keys", makes unique, sort. keeps that data. finally pass data to generate (switch-cases) file for controller. implementation will reside in external files, referenced from the primary one. make helper to generate from a list. and another to generate keys.
class ExampleReader
{
	
	
		public static function do_example(args:Array<String>)
	{
		var cmd:String = args.slice(1).join(" ");
		trace('command is: $cmd');
		println("validating command syntax...");
		var fullregex = ~/^(?'command'[a-zA-Z]+($|\s+)){1,1}(?'subs'(([a-zA-Z]+)($|\s+))*)(?'configs'(-[A-Za-z0-9]+:[A-Za-z0-9]+($|\s+))*)$/i;

		var ok:Bool = fullregex.match(cmd);
		if (!ok) { 
			println("command syntax is wrong"); Aux.help_cmd_syntax();
			return; 
			} 
		else {
			println("command syntax ok");
			println("Appending command to examples.txt");
			var file = sys.io.File.append("./examples.txt");
			file.writeString(cmd);
			file.close();
		}
	}
	
	static function cmd_cases (cmds:Array<String>):String
	{
		for (item in cmds)
		{
			println(item); // TODO
		}
		return "str";
	}
	
}