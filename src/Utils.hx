import Sys.*;
using StringTools;
import thx.core.*;


	
class Utils
{
	public static inline function todo(?str:String):String
{
	var res = 'FEATURE NOT YET IMPLEMENTED';
	if (str != null) res+=": "+str;
	return res;
	
}

	static function subfiles(dir:String):Array<Path> {
	var list = [];
	var files = readDirectory(dir);
	for (f in files)
		{
			var file = dir+"/"+f;
		if (!isDirectory(file))
			list.push(new Path(file));
		}	 
		return list;
	}


	public static function cmp(a,b) : Int {
		if (a < b) return -1;
		else if (a > b) return 1;
		return 0;
	}
public static function list_files_sub (path :Path ) : Array<Path>
{
	 var result:Array<Path> = [];
	 //determine if argument is file or directory.
	 var readpath = if (isDirectory(path.toString())) path.toString() else path.dir;

	 var files = readDirectory(readpath);
	 for (f in files)
	 {
		var	full = new Path(readpath+"/"+f);
		var	subfiles = [];
		var fullstr = full.toString();
		 if (!isDirectory(fullstr)) 
		{ 
			var file = full;			 
			result.push(file); 
		}
		else
		{
			 var subfiles = list_files_sub(full); //dir
			 result = result.concat(subfiles); // add subfiles. (flattened)
		}
	 }
 
	 return result;
 }

	// using version
	  public static function assert(o:Object, cond : Bool, ?pos : haxe.PosInfos ) {
	  if( !cond ) {
		  haxe.Log.trace("Assert in "+pos.className+"::"+pos.methodName,pos);
		}
	}
}