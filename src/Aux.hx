import Sys.*;

class Aux
{
	
 public static function help_cmd_syntax() {
	 println("\nCommand help:");
	 println("\tcommands are words seperated by space");
	 println("\tthe first word is the main command, the following are sub commands");
	 println("\tfor example: read book");
	 println("\tto add parameters use  -param:value -param2:other_value");
	 println("\tfor example: read book -title:howdy -id:1234");
	 
 }
 
 
 
	static public function show_args(args:Array<String>)
	{
		print("Received arguments: ");
		for ( x in args)
		{
			print(x+" ");
		}
		println("\n");
	}
	
	

	public static function usage()
	{
		var str = "
usage:  vmvc gen\n\tvmvc example";

		println(str);

	}
 
}